<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include "includes/head.php"; ?>
    <link rel="stylesheet" href="css/nospistes.css">
    <title>Nos Pistes</title>
</head>

<body>
    <header>
        <?php include "includes/header.php"; ?>
    </header>

    <div class="container-fluid bienvenue">
        <div class="row">
            <div class="col-12 text-center">
                <h2>Bienvenue au Complexe Spacebowl</h2>
            </div>
            <div class="col-12 text-center">
                <img src="img/background/sabre1.png" width="30%">
                <img src="img/background/sabre2.png" width="30%">
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col12 text-center bg-light ">
                <h2>1000 M² d'espace de jeux</h2>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 d-flex align-items-center">
                <img src="img/nospistes/img1.png">
            </div>
            <div class="col-md-6">
                <h3 class="titre1">5 pistes à thèmes star wars</h3>
                <p>Devenez l'empereur de la piste en combattant</p>
                <p>l'équipe adverse !</p>
                <p>Faites resurgir la force qui est en vous !</p>
                <p>Lorem ipsum dolor sit amet, consectetur </p>
                <p>Praesent ac rhoncus eros,vitae porttitor enim.</p>
                <p>Fusce urna nisl, mattis at bibendum ac, finibus</p>
                <p>vitae sapien.aliquet lectus.diam.</p>
                <p>Etiam in lectus vulputate, ullamcorper nisi eu, </p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-6">
                <h3 class="titre1">5 pistes à thèmes horreur</h3>
                <p>Devenez l'empereur de la piste en combattant</p>
                <p>l'équipe adverse !</p>
                <p>Faites resurgir la force qui est en vous !</p>
                <p>Lorem ipsum dolor sit amet, consectetur </p>
                <p>Praesent ac rhoncus eros,vitae porttitor enim.</p>
                <p>Fusce urna nisl, mattis at bibendum ac, finibus</p>
                <p>vitae sapien.aliquet lectus.diam.</p>
                <p>Etiam in lectus vulputate, ullamcorper nisi eu, </p>
            </div>
            <div class="col-sm-12 col-md-6 d-flex align-items-center">
                <img src="img/nospistes/img2.png">
            </div>

        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 d-flex align-items-center">
                <img src="img/nospistes/img3.png">
            </div>
            <div class="col-6">
                <h3 class="titre1">5 pistes à thèmes marvel</h3>
                <p>Devenez l'empereur de la piste en combattant</p>
                <p>l'équipe adverse !</p>
                <p>Faites resurgir la force qui est en vous !</p>
                <p>Lorem ipsum dolor sit amet, consectetur </p>
                <p>Praesent ac rhoncus eros,vitae porttitor enim.</p>
                <p>Fusce urna nisl, mattis at bibendum ac, finibus</p>
                <p>vitae sapien.aliquet lectus.diam.</p>
                <p>Etiam in lectus vulputate, ullamcorper nisi eu, </p>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col12 text-center bg-light ">
                <h2>Espace arcade</h2>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center ">
                <img src="img/nospistes/img4.png">
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center ">
                <h3 class="titre1">20 bornes d'arcade</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>20 bornes d'arcade où vous pourrez vous amuser en toute liberté avec vos amies Lorem ipsum dolor sit
                    amet, consectetur </p>
                <p>adipiscing elit.</p>
                <P>Praesent ac rhoncus eros,vitae porttitor enim. Fusce urna nisl, mattis at bibendum ac, finibus vitae
                    sapien.aliquet lectus.diam.</P>
                <P>Etiam in lectus vulputate, ullamcorper nisi eu, Nulla nec risus at felis hendrerit rhoncus eget at
                    Donec facilisis ipsum et mi </P>
                <P>posuere</P>
                <P> convallis nec Nunc vel justo ex. In dolor arcu,quis lacus.posuere sed feugiat sodales, blandit ut
                    lacus.</P>
            </div>

        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col12 text-center bg-light ">
                <h2>Espace détente</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center ">
                <img src="img/nospistes/img5.png">
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center ">
                <h3 class="titre1">Prenez une pause dans notre bar</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>20 bornes d'arcade où vous pourrez vous amuser en toute liberté avec vos amies Lorem ipsum dolor sit
                    amet, consectetur </p>
                <p>adipiscing elit.</p>
                <P>Praesent ac rhoncus eros,vitae porttitor enim. Fusce urna nisl, mattis at bibendum ac, finibus vitae
                    sapien.aliquet lectus.diam.</P>
                <P>Etiam in lectus vulputate, ullamcorper nisi eu, Nulla nec risus at felis hendrerit rhoncus eget at
                    Donec facilisis ipsum et mi </P>
                <P>posuere</P>
                <P> convallis nec Nunc vel justo ex. In dolor arcu,quis lacus.posuere sed feugiat sodales, blandit ut
                    lacus.</P>
            </div>
        </div>
    </div>


    <footer>
        <?php include "includes/footer.php"; ?>
    </footer>
</body>

</html>