<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include "includes/head.php"; ?>
    <link rel="stylesheet" href="css/accueil.css">
    <title>Accueil</title>
</head>

<body>
    <header>
        <?php include "includes/header.php"; ?>
    </header>

    <div class="container-fluid bienvenue">
        <div class="row">
            <div class="col-12 text-center">
                <h1>Bienvenue au Complexe Spacebowl</h1>
            </div>
            <div class="col-12 text-center">
                <img src="img/background/sabre1.png" width="30%">
                <img src="img/background/sabre2.png" width="30%">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-4 text-center">
                <img src="img/background/logo1.png">
                <h3>15</h3>
                <p>Pistes de Bowling</p>
            </div>
            <div class="col-sm-4 col-md-4 text-center">
                <img src="img/background/logo2.png">
                <h3>1</h3>
                <p>Bar</p>
            </div>
            <div class="col-sm-4 col-md-4 text-center">
                <img src="img/background/logo3.png">
                <h3>20</h3>
                <p>Bornes d'arcades</p>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row">
            <div class="col12 text-center bg-light ">
                <h2>Aperçu</h2>
            </div>
        </div>
    </div>

    <video class="w-100" autoplay loop muted>
        <source src="video/video1.mp4" type="video/mp4" />
    </video>


    <div class="container-fluid">
        <div class="row">
            <div class="col12 text-center bg-light ">
                <h2>évènements</h2>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row bg-light">
            <div class="col-sm-12 col-md-6">
                <div class="card border-0">
                    <div class="card-body card-body1">
                        <h6>ENTERREMENT DE VIE DE</h6>
                        <h6>JEUNE FILLE / GARÇON</h6>
                        <h6>19€ par pers.</h6>
                        <p class="card-text-align">pour faire de cette journée un moment inoubliable
                        </p>
                        <a href="#" class="btn btn-danger" role="button" data-bs-toggle="button">Réservez</a>
                    </div>
                </div>
            </div>


            <div class="col-sm-12 col-md-6">
                <div class="card border-0">
                    <div class="card-body card-body1">
                        <h6>ENTERREMENT DE VIE DE</h6>
                        <h6>JEUNE FILLE / GARÇON</h6>
                        <h6>19€ par pers.</h6>
                        <p class="card-text-align">pour faire de cette journée un moment inoubliable
                        </p>
                        <a href="#" class="btn btn-danger" role="button" data-bs-toggle="button">Réservez</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="carousel/img1.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="carousel/img2.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="carousel/img3.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="carousel/img4.png" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="carousel/img5.png" class="d-block w-100" alt="...">
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center bg-light text-center">
                <a href="#" class="btn btn-danger" role="button" data-bs-toggle="button">
                    <h2>Découvez nos pistes</h2>
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 bg-danger">
            </div>
        </div>
    </div>

    <footer>
        <?php include "includes/footer.php"; ?>
    </footer>
</body>

</html>