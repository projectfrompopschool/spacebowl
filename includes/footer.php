<div class="container-fluid">
    <div class="row bg-light">
        <div class="col-md-4 col-sm-12 d-flex justify-content-center align-items-center">
            <img src="img/logofooter.png" width="40%" height="70%">
        </div>
        <div class="col-md-4 col-sm-12">
            <h3>Horaires</h3>
            <p>Lundi au vendredi</p>
            <p>12H - Minuit</p>
            <p>Samedi à Dimanche</p>
            <p>12H - Minuit</p>
        </div>
        <div class="col-md-4 col-sm-12">
            <h3>Autres</h3>
            <p>Politiques de confidentialité</p>
            <p>Mentions légales</p>
            <p>Contact</p>
        </div>
    </div>
</div>