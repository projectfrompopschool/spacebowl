<?php 


namespace App\Date;



class Month {

    private $months = [ 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    private $month;
    private $year;


    public function __construct(int $month, int $year) 
    {
        if($month <1 || $month > 12) {
            throw new \Exception(message: "Le mois $month n'est pas valide");
        }

        if($year <1970) 
        {
            throw new \Exception(message: "L'année est inférieur à 1970");
        } 
        $this->month = $month;
        $this->year = $year;
    }

    /** retourne le mois en toute lettres (ex : Mars 2018)
     * @return string
     * 
     */
    public function toString():string {
        return $this->months[$this->month - 1] . '' . $this->year;
    }




}